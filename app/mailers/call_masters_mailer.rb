class CallMastersMailer < ApplicationMailer
  default from: 'SobolevSergey9999@gmail.com'

  def call_masters_message(message)
    @call_master = message
    mail_first = 'SobolevSergey9999@gmail.com'
    mail_second = 'sergrpm@mail.ru'
    mail to: [mail_first, mail_second], subject: 'Добрый день'
  end
end
