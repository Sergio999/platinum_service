$(document).on('scroll', function () {
  // if the scroll distance is greater than 100px
  var top = $('.mainTop');
  var menu = $('#templatemo_top');
  if ($(window).scrollTop() > 50) {
    top.fadeOut('slow');
    menu.css({
      'background-color':'white'
    })
   };
  if ($(window).scrollTop() < 50) {
    // do something
    var top = $('.mainTop');
    top.fadeIn('slow');
    menu.css({
      'background-color':'#f7fbfc'
    })
  };
})
$(window).load(function() {
  $('#slider').nivoSlider();
});

$(document).ready(function(){
  var up = $('.mainMenu #up');
  var logo = $('.logo');
  up.click(function(){
    $('body, html').animate({
        scrollTop: 0
  },700);
  });
  logo.click(function(){
    $('body, html').animate({
        scrollTop: 0
  },700);
  });

  setTimeout(function() {
    var flash = $('.my-flash');
    flash.fadeOut('slow');
  },7000);
})

$(window).load(function(){
  var $page = $('html, body');
  var flash_error = $('#error_explanation');
  if (flash_error.length == 1){
    var scroll_error = flash_error.offset().top;
    $page.animate({
      scrollTop: scroll_error - 100
    });
  };
  var alert_info = $('.alert.alert-info');
  if (alert_info.length == 1) {
    var scroll_alert_info = alert_info.offset().top;
    $page.animate({
       scrollTop: scroll_alert_info - 150
    }, 700 )
  };
  
})
