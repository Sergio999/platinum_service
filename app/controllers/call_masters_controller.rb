class CallMastersController < ApplicationController
  def new
    @call_master = CallMaster.new
  end

  def create
    @message = Message.new
    @call_master = CallMaster.new(call_masters_params)
    if @call_master.valid?
      CallMastersMailer.call_masters_message(@call_master).deliver
      redirect_to root_path, notice: 'Спасибо! Мы Вам скоро перезвоним'
    else
      render 'static_pages/index'
    end
  end

  private

  def call_masters_params
    params.require(:call_master).permit(:name, :phone, :breaking, :address)
  end
end
