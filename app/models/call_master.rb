class CallMaster
  include ActiveModel::Model

  attr_accessor :name, :phone, :breaking, :address

  PHONE_NUMBER = /\A\d{3}-\d{3}-\d{4}\z/
  validates :name, presence: true, length: { maximum: 20 }
  validates :phone, presence: true, numericality: true, length: { minimum: 7, maximum: 15 }
  validates :address, length: { maximum: 400 }
  validates :breaking, presence: true
end
